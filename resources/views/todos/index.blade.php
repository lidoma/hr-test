@php
    /**
     * @var \Illuminate\Database\Eloquent\Collection<int, App\Models\Todo> $todos
     */
@endphp

@extends('layouts.main')

@section('content')
    <div class="flex items-center justify-center font-sans">
        <div class="bg-white rounded shadow p-6 mt-8 w-3/4">
            <div class="mb-10">
                <h1 class="text-gray-900">Todo List</h1>
                <form action="{{ route('todos.store') }}" method="POST" class="flex mt-4">
                    @csrf
                    <div class="flex-grow mr-4">
                        <input
                            class="block shadow appearance-none border rounded w-full py-2 px-3 text-gray-900"
                            name="description" placeholder="Add Todo"
                            value="{{ old('description') }}">

                        @error('description')
                            <span class="text-red-600 text-sm mt-40">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <button class="flex-shrink-0 p-2 border-2 rounded text-teal-500 border-teal-500 hover:text-white hover:bg-teal-500">
                        Add
                    </button>
                </form>
            </div>
            <div>
                @foreach($todos as $todo)
                    <div class="flex mb-4 items-center">

                        <p @class([
                            'w-full text-gray-900',
                            'line-through text-green-500' => $todo->isCompleted(),
                        ])>{{ $todo->description }}</p>

                        <form action="{{ route('todos.toggle', $todo) }}" method="POST" class="flex-shrink-0 ml-4 mr-2">
                            @csrf

                            @if($todo->isCompleted())
                                <button
                                    class="p-2 border-2 rounded hover:text-white text-gray-500 border-gray-500 hover:bg-gray-500">
                                    Not Done
                                </button>
                            @else
                                <button
                                    class="p-2 border-2 rounded hover:text-white text-green-500 border-green-500 hover:bg-green-500">
                                    Done
                                </button>
                            @endif
                        </form>

                        <form action="{{ route('todos.destroy', $todo) }}" method="POST" class="flex-shrink-0 ml-2">
                            @csrf
                            @method('DELETE')
                            <button type="submit"
                                class="p-2 border-2 rounded text-red-500 border-red-500 hover:text-white hover:bg-red-500">
                                Remove
                            </button>
                        </form>

                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
