<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Todo extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'description',
    ];

    /**
     * Determine if the todo is completed.
     *
     * @return bool
     */
    public function isCompleted(): bool
    {
        return ! is_null($this->done_at);
    }

    /**
     * Toggle the todo state.
     *
     * @return $this
     */
    public function toggle(): static
    {
        $this->isCompleted() ? $this->markAsPending() : $this->markAsCompleted();

        return $this;
    }

    /**
     * Mark the todo state as pending.
     *
     * @return bool
     */
    public function markAsPending(): bool
    {
        return $this->forceFill([
            'done_at' => null,
        ])->save();
    }

    /**
     * Mark the todo state as completed.
     *
     * @return bool
     */
    public function markAsCompleted(): bool
    {
        return $this->forceFill([
            'done_at' => $this->freshTimestamp(),
        ])->save();
    }
}
