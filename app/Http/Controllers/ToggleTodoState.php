<?php

namespace App\Http\Controllers;

use App\Models\Todo;

class ToggleTodoState
{
    /**
     * Toggle the state of the specified todo.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke(int $id)
    {
        tap(Todo::query()->findOrFail($id), fn(Todo $todo) => $todo->toggle());

        return to_route('todos.index');
    }
}
