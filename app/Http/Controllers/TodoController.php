<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTodoRequest;
use App\Models\Todo;

class TodoController
{
    /**
     * Display a listing of the todos.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('todos.index', [
            'todos' => Todo::query()->latest()->get(),
        ]);
    }

    /**
     * Store a newly created todo in storage.
     *
     * @param  \App\Http\Requests\StoreTodoRequest  $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreTodoRequest $request)
    {
        Todo::create($request->validated());

        return to_route('todos.index');
    }

    /**
     * Remove the specified todo from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $id)
    {
        tap(Todo::query()->findOrFail($id), fn(Todo $todo) => $todo->delete());

        return to_route('todos.index');
    }
}
