<?php

use App\Http\Controllers\TodoController;
use App\Http\Controllers\ToggleTodoState;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return to_route('todos.index');
});

Route::resource('todos', TodoController::class)->only(['index', 'store', 'destroy']);
Route::post('todos/{todo}/toggle', ToggleTodoState::class)->name('todos.toggle');
