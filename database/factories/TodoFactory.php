<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Todo>
 */
class TodoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'description' => $this->faker->realText(60),
            'done_at' => $this->faker->optional(.3)->dateTimeThisMonth(),
        ];
    }

    /**
     * Indicate that the model has been marked as done.
     *
     * @return static
     */
    public function done(): static
    {
        return $this->state(fn() => [
            'done_at' => $this->faker->dateTimeThisMonth(),
        ]);
    }
}
